import pandas as pd 
import numpy as np
# import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.naive_bayes import MultinomialNB
from sklearn.metrics import accuracy_score
from sklearn.preprocessing import LabelEncoder

data = pd.read_csv(r'Breast_cancer.csv')
data = data.drop(['id','Unnamed: 32'] , axis=1);
# for x in range(len(data['diagnosis'])):
# 	if ((data['diagnosis'][x]) == 'B'):
# 		data = data.replace(data['diagnosis'][x] , 1)
# 	elif ((data['diagnosis'][x]) == 'M'):
# 		data = data.replace(data['diagnosis'][x] , 2)

enc = LabelEncoder()
data['diagnosis'] = enc.fit_transform(data['diagnosis'].values)


x = data.drop(columns='diagnosis')
y = data['diagnosis']

x_train,x_test,y_train,y_test = train_test_split(x,y,test_size=0.3, random_state=43)


bayes = MultinomialNB().fit(x_train,np.ravel(y_train))
result = bayes.predict(x_test)
acuration = accuracy_score(y_test , result)
print (result)
print(acuration)